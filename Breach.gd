extends Area2D

onready var Player = $"../Player"
var quantity = 1

func _ready():
	$"Sprite/AnimationPlayer".play("succ")

func _process(delta):
	if Player in get_overlapping_bodies():
		if Input.is_action_just_pressed("interact") and !Player.is_interacting:
			Player.is_interacting = true
			$"InteractionTimer".start()

func _on_InteractionTimer_timeout():
	Player.is_interacting = false
	$"..".current_breaches.erase(position)
	queue_free()

func _on_OxygenTimer_timeout():
	PlayerResources.resources_dict["oxygen"] -= 1
	quantity += 1
	if PlayerResources.resources_dict["oxygen"] <= 0:
		Transition.fade_to_scene("res://GameOver.tscn")
