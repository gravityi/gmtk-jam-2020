extends KinematicBody2D

export var movement_speed = 10

var horizontal = 0
var vertical = 0
var jump_height = 0
var horizontal_speed = 0
var falling_speed = 0
var is_interacting = false

func _ready():
	horizontal = 0
	jump_height = -3 * movement_speed
	horizontal_speed = 20
	
func _process(delta):
	if Input.is_action_just_pressed("ui_left"):
		horizontal -= horizontal_speed
	if Input.is_action_just_pressed("ui_right"):
		horizontal += horizontal_speed

	if Input.is_action_just_released("ui_left"):
		horizontal += horizontal_speed
	if Input.is_action_just_released("ui_right"):
		horizontal -= horizontal_speed

	if Input.is_action_just_pressed("ui_up"):
		jump()
	if Input.is_action_just_pressed("ui_down") and vertical < 10:
		vertical = 10

	if is_on_ceiling():
		vertical = 0

	if is_interacting: $"Sprite/AnimationPlayer".play("interacting")
	elif horizontal != 0 and is_on_floor(): $"Sprite/AnimationPlayer".play("run")
	elif horizontal == 0 and is_on_floor(): $"Sprite/AnimationPlayer".play("idle")
	else: $"Sprite/AnimationPlayer".play("jump")

	if horizontal < 0: 
		$"Sprite".flip_h = false
	elif horizontal > 0: 
		$"Sprite".flip_h = true

	#"Gravity"
	if !is_on_floor():
		vertical += delta * falling_speed * movement_speed
		falling_speed += 0.1
	else:
		falling_speed = 0

	if !is_interacting: move_and_slide(Vector2(horizontal, vertical)*movement_speed, Vector2.UP)

func jump():
	vertical = jump_height
	falling_speed = 0
