extends Node

signal loaded_resource

const LoadingScreen = preload("res://transition/LoadingScreen.tscn")

onready var FaderScript = load("res://transition/Fader.gd")
onready var resource_queue = load("res://transition/resource_queue.gd").new()

var loading
func _ready():
	resource_queue.start()
	fade_in()

# Motion Design
#	yield(get_tree().create_timer(2), "timeout")
#	fade_to_scene("purposefullyheavyscene.tscn")

func fade_in():
	var Fader = TextureRect.new()
	Fader.set_script(FaderScript)
	Fader.fade_out = false
	Fader.modulate[3] = 1
	get_tree().current_scene.add_child(Fader)
	
func fade_out():
	var Fader = TextureRect.new()
	Fader.set_script(FaderScript)
	Fader.fade_out = true
	Fader.modulate[3] = 0
	get_tree().current_scene.add_child(Fader)

func fade_to_scene(scene):
	resource_queue.queue_resource(scene)
	var Fader = TextureRect.new()
	Fader.set_script(FaderScript)
	Fader.fade_out = true
	Fader.modulate[3] = 0
	Fader.next_scene = scene
	get_tree().current_scene.add_child(Fader)

func on_fade_out(scene):
	if resource_queue.is_ready(scene):
	# warning-ignore:return_value_discarded
		get_tree().change_scene(scene)
		call_deferred("fade_in")
		emit_signal("loaded_resource")
	elif loading == null:
		var NewLoadingScreen = LoadingScreen.instance()
# warning-ignore:return_value_discarded
		connect("loaded_resource", NewLoadingScreen, "queue_free")
		get_viewport().add_child(NewLoadingScreen)
		loading = scene
		
