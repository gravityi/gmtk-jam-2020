extends Timer

func _ready():
	randomize()
	wait_time = rand_range(5, 10)
	PlayerResources.asteroid_incoming = true

func _on_Asteroid_timeout():
	PlayerResources.deal_damage()
	queue_free()
