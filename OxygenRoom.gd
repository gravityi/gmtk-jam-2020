extends Area2D

func _on_OxygenRoom_body_entered(body):
	if body.is_in_group("player"):
		$"ActionTimer".start()

func _on_OxygenRoom_body_exited(body):
	if body.is_in_group("player"):
		$"ActionTimer".stop()

func _on_ActionTimer_timeout():
	PlayerResources.resources_dict["oxygen"] = clamp(PlayerResources.resources_dict["oxygen"] + 1, 0, PlayerResources.resource_limits["oxygen"])
