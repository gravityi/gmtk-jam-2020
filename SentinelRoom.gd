extends Area2D

func _on_SentinelRoom_body_entered(body):
	PlayerResources.shielded = true

func _on_SentinelRoom_body_exited(body):
	PlayerResources.shielded = false
